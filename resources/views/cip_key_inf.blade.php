@extends('layout.main')

@section('main.body')

    <div class="container py-5">
        <h1>Generate cip_key_inf</h1>
        <hr>
        <pre style="white-space: pre-wrap; word-break: break-word;">{{ $cipKeyInf }}</pre>
    </div>

@endsection
