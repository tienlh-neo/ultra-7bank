@extends('layout.main')

@section('main.body')
    <div class="container py-5">
        <h1>JWS Generator</h1>
        <hr>

        <h3>Hướng dẫn: </h3>
        <pre>key: value</pre>
        <ul>
            <li>Mỗi <code>key: value</code> phân biệt bằng cách xuống dòng</li>
            <li>Chú ý ghi đúng cú pháp trên sẽ không lỗi. Chứ lỗi không biết fix à :'(</li>
        </ul>

        <hr>
        <div class="form-group">
            <label class="control-label">HEADER</label>
            <textarea id="header" name="header" class="form-control monospace" rows="3">{{ $defaultHeader }}</textarea>
        </div>
        <div class="form-group">
            <label class="control-label">PAYLOAD</label>
            <textarea name="payload" id="payload" class="form-control monospace" rows="10">{{ $defaultPayload }}</textarea>
        </div>
        <div class="form-group">
            <label class="control-label">SECRET</label>
            <input type="text" id="secret" name="secret" class="form-control monospace" value="{{ $defaultSecret }}">
        </div>
        <div class="form-group">
            <button id="submit" type="submit" class="btn btn-primary">GENERATE</button>
        </div>
        <hr>
        <h2>TOKEN</h2>
        <pre style="white-space: pre-wrap; word-break: break-word;" id="token"></pre>
    </div>
@endsection

@section('main.script')
<script>
    $(function () {
        $('#submit').click(function () {


            fetch('{{ route('tools.jws_generator') }}', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                body: JSON.stringify({
                    header: $('#header').val(),
                    payload: $('#payload').val(),
                    secret: $('#secret').val(),
                })
            }).then(res => res.json()).then(res => $('#token').html(res.token))
        });
    });
</script>
@endsection
