@extends('layout.main')

@section('main.body')
    <div class="container-fluid py-5">

        <h1>Decrypt sec_inf</h1>
        <hr>

        <div class="row">
            <div class="col-md-6">
                <form>
                    <div class="form-group">
                        <label>COMMON KEY</label>
                        <input type="text" maxlength="64" id="common_key" name="common_key" placeholder="String 64 characters (HEX)" class="form-control" value="{{ request('common_key') }}">
                    </div>
                    <div class="form-group">
                        <label>sec_inf</label>
                        <textarea id="sec_inf" class="form-control monospace" id="sec_inf" name="sec_inf" rows="5">{{ request('sec_inf') }}</textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">DECODE</button>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                @foreach ($data as $key => $value)
                <strong>{{ $key }}:</strong>
                <pre style="word-break: break-all; white-space: pre-wrap;">{{ var_dump($value) }}</pre>
                @endforeach
            </div>
        </div>
    </div>
@endsection
