@extends('layout.main')

@section('main.body')

    <div class="container py-5">

        <h1>RSA KeyPair</h1>
        <hr>

        <div class="form-group py-3">
            <strong>PUBLIC</strong>
            <pre style="white-space: pre-wrap; word-break: break-word;">{{ $keyPair['public'] ?? '' }}</pre>
        </div>
        <div class="form-group py-3">
            <strong>PRIVATE KEY</strong>
            <pre style="white-space: pre-wrap; word-break: break-word;">{{ $keyPair['private'] ?? '' }}</pre>
        </div>

        <hr>

        <h3>Generate new RSA KeyPair</h3>
        <form action="{{ route('tools.rsa_key_pair') }}" method="POST">
            @csrf
            <button type="submit" class="btn btn-primary">GENERATE</button>
        </form>
    </div>

@endsection
