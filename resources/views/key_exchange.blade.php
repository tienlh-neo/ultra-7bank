@extends('layout.main')
@section('main.body')

    <div class="container-fluid py-5">

        <h1>Key Exchange</h1>
        <pre>{{ config('ultra.sevenbank.base_uri') }}api/v1/key_exchange</pre>
        <hr>

        <div class="row">
            <div class="col-md-6">
                @include('components.fields', compact('fields', 'default'))
                <div class="form-group row">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <button id="request" class="btn btn-primary">SEND REQUEST</button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h3>RESPONSE</h3>
                <pre id="response" style="white-space: pre-wrap; word-break: break-word;"></pre>
            </div>
        </div>
    </div>
@endsection
@section('main.script')
    <script>
        $(function () {

            var _fields = [
                'cip_key_inf',
                'access_token',
                'tran_time',
                'audi_id',
                'audi_subid',
                'center_id',
                'atm_id',
                'dest_id',
                'accept_id',
            ];

            var _isRequesting = false;

            $('#request').click(function () {
                if (_isRequesting) {
                    return;
                }

                toggleRequesting(_isRequesting = true);

                var data = {};

                _fields.forEach(field => {
                    data[field] = $('#fields_' + field).val();
                });

                console.log(data);

                fetch('{{ route('transactions.key_exchange') }}', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    },
                    body: JSON.stringify(data),
                }).then(function (res) {
                    return res.json();
                }).then(function (res) {
                    $('#response').html('');
                    Object.keys(res).forEach(function (key) {

                        var value = res[key];
                        var type  = 'mixed';

                        if (typeof value === 'object') {
                            if (value.length) {
                                type = 'array';
                                value = value.map(function (item) {
                                    return ' - ' + item;
                                }).join('\n');
                            } else {
                                type = 'object';
                                value = Object.keys(value).map(function (k) {
                                    return ' - ' + k + ': ' + JSON.stringify(value[k]);
                                }).join('\n');
                            }
                        }

                        $('#response').append(key.toUpperCase() + ' (' + type + '):\n' + value + '\n\n');
                    });
                }).finally(function () {
                    toggleRequesting(_isRequesting = false);
                });
            });

            function toggleRequesting(requesting) {
                if (requesting) {
                    $('#request').html('Requesting...');
                } else {
                    $('#request').html('SEND REQUEST');
                }
            }
        });
    </script>
@endsection
