@extends('layout.main')

@section('main.body')

    <div class="container-fluid py-5 mb-lg-5">
        <h1>Revoke Access Token</h1>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <h3>Custom Request (PARAMS)</h3>
                <br>

                <div class="form-group row">
                    <label class="col-md-3">client_id</label>
                    <div class="col-md-8">
                        <input type="text" id="client_id" class="form-control monospace" value="sevenbank">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3">grant_type</label>
                    <div class="col-md-8">
                        <input type="text" id="grant_type" class="form-control monospace" value="client_credentials">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3">client_assertion_type</label>
                    <div class="col-md-8">
                        <input type="text" id="client_assertion_type" class="form-control monospace" value="urn:ietf:params:oauth:client-assertion-type:jwt-bearer">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3">JWS <br><small><a href="{{ route('tools.jws_generator') }}" target="_blank">Create custom JWS here</a></small></label>
                    <div class="col-md-8">
                        <textarea id="client_assertion" class="form-control monospace" name="jws_token" rows="5">{{ $defaultJWS ?? '' }}</textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-8">
                        <button id="request" class="btn btn-primary">SEND REQUEST</button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h3>RESPONSE</h3>
                <pre id="response" style="white-space: pre-wrap; word-break: break-word;"></pre>
            </div>
        </div>
    </div>

@endsection
@section('main.script')
<script>
    $(function () {

        var _isRequesting = false;

        $('#request').click(function () {
            if (_isRequesting) {
                return;
            }

            toggleRequesting(_isRequesting = true);

            var data = {
                client_id: $('#client_id').val(),
                grant_type: $('#grant_type').val(),
                client_assertion_type: $('#client_assertion_type').val(),
                client_assertion: $('#client_assertion').val(),
            };

            fetch('{{ route('access_token.send_revoke') }}', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                body: JSON.stringify(data)
            }).then(function (res) {
                return res.json();
            }).then(function (res) {
                $('#response').html('');
                Object.keys(res).forEach(function (key) {

                    var value = res[key];
                    var type  = 'mixed';

                    if (typeof value === 'object') {
                        if (value.length) {
                            type = 'array';
                            value = value.map(function (item) {
                                return ' - ' + item;
                            }).join('\n');
                        } else {
                            type = 'object';
                            value = Object.keys(value).map(function (k) {
                                return ' - ' + k + ': ' + JSON.stringify(value[k]);
                            }).join('\n');
                        }
                    }

                    $('#response').append(key.toUpperCase() + ' (' + type + '):\n' + value + '\n\n');
                });
            }).finally(function () {
                toggleRequesting(_isRequesting = false);
            });
        });

        function toggleRequesting(requesting) {
            if (requesting) {
                $('#request').html('Requesting...');
            } else {
                $('#request').html('SEND REQUEST');
            }
        }
    });
</script>
@endsection
