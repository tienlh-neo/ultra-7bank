@extends('layout.main')

@section('main.body')

    <div class="container py-5">

        <div class="py-2">
            <a href="{{ route('tools.jws_generator') }}" class="btn btn-primary">TRY ANOTHER</a>
        </div>

        <div class="row py-5">
            <div class="col-md-12">
                <pre style="white-space: pre-wrap; word-break: break-word;">{{ $token ?? '' }}</pre>
            </div>
        </div>

    </div>

@endsection
