<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('index') }}">SevenBank Ultra Tool</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="accessTokenDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Access Token
                    </a>
                    <div class="dropdown-menu" aria-labelledby="accessTokenDropdown">
                        <a class="dropdown-item" href="{{ route('access_token.issue') }}">Issue AccessToken</a>
                        <a class="dropdown-item" href="{{ route('access_token.revoke') }}">Revoke AccessToken</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="accessTokenDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Transactions
                    </a>
                    <div class="dropdown-menu" aria-labelledby="accessTokenDropdown">
                        <a class="dropdown-item" href="{{ route('transactions.key_exchange') }}">key_exchange</a>
                        <a class="dropdown-item" href="{{ route('transactions.transaction_detail') }}">transaction_detail</a>
                        <a class="dropdown-item" href="{{ route('transactions.deposit') }}">deposit</a>
                        <a class="dropdown-item" href="{{ route('transactions.withdrawal') }}">withdraw</a>
                        <a class="dropdown-item" href="{{ route('transactions.withdrawal_cancel') }}">withdraw_cancel</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="toolsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Tools
                    </a>
                    <div class="dropdown-menu" aria-labelledby="toolsDropdown">
                        <a class="dropdown-item" href="{{ route('tools.qr_code') }}">QR Code</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('tools.jws_generator') }}">JWS Generator</a>
                        <a class="dropdown-item" href="{{ route('tools.jws_inspector') }}">JWS Inspector</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('tools.rsa_key_pair') }}">RSA Key Pair</a>
                        <a class="dropdown-item" href="{{ route('tools.aes_common_key') }}">AES Common Key</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('tools.cip_key_inf') }}">cip_key_inf</a>
                        <a class="dropdown-item" href="{{ route('tools.sec_inf') }}">sec_inf</a>
                        <a class="dropdown-item" href="{{ route('tools.decode_sec_inf') }}">Decrypt sec_inf</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
