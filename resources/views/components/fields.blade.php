@foreach ($fields as $field)
    <div class="form-group row">
        <label class="col-md-4">{{ $field[0] }}<br><small class="text-info">{{ $field[2] }}</small></label>
        <div class="col-md-8">
            @if (is_null($field[1]))
                <textarea id="fields_{{ $field[0] }}" rows="3" class="monospace form-control" placeholder="{{ $field[2] }}">{{ $default[$field[0]] ?? '' }}</textarea>
            @else
                <input type="text" id="fields_{{ $field[0] }}" maxlength="{{ $field[1] }}" class="monospace form-control" placeholder="{{ $field[2] }}" value="{{ $default[$field[0]] ?? '' }}">
            @endif
        </div>
    </div>
@endforeach
