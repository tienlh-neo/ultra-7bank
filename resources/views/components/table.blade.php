<table class="table">
    @foreach ($rows as $key => $value)
        <tr>
            <th style="width: 100px;">{{ $key }}</th>
            <td style="word-break: break-all">{{ $value }}</td>
        </tr>
    @endforeach
</table>
