@extends('layout.main')

@section('main.body')

    <div class="container py-5">

        <h1>AES (Common Key)</h1>
        <hr>

        <div class="form-group py-3">
            <strong>AES (Common Key)</strong>
            <pre style="white-space: pre-wrap; word-break: break-word;">{{ $aes }}</pre>
        </div>
        <hr>
        <h3>Generate new AES (Common Key)</h3>
        <form action="{{ route('tools.aes_common_key') }}" method="POST">
            @csrf
            <button type="submit" class="btn btn-primary">GENERATE</button>
        </form>
    </div>

@endsection
