@extends('layout.main')

@section('main.body')
    <div class="container py-5">
        <h1>Brief</h1>
        <hr>

        <h3>1. JWS Client Credentials</h3>
        <pre>{{ config('ultra.jws.secret') }}</pre>
    </div>
@endsection
