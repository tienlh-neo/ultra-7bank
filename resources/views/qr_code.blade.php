@extends('layout.main')

@section('main.body')

    <div class="container py-5">
        <h1>QR Code Generator</h1>
        <hr>

        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('tools.qr_code') }}">
                    <div class="form-row">
                        <div class="form-group col-md-8">
                            <input type="text" maxlength="23" name="code" class="form-control" value="{{ request('code') }}">
                        </div>
                        <div class="form-group col-md-4">
                            <button type="submit" class="btn btn-outline-primary">Generate</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="pt-3">
                    <img src="{{ $qrCode ?: 'https://via.placeholder.com/300?text=QR CODE HERE' }}" alt="">
                </div>
            </div>
        </div>
    </div>

@endsection
