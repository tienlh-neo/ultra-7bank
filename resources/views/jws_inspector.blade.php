@extends('layout.main')

@section('main.body')

    <div class="container py-5">

        <h1>JWS Inspector</h1>
        <hr>

        <form>
            <div class="form-group">
                <textarea name="jws" id="" cols="30" rows="5" class="form-control">{{ request('jws') }}</textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">INSPECT</button>
            </div>
        </form>

        <hr>

        @if ($inspector)

            <div class="form-group mb-3">
                <strong>HEADER</strong>
                @if (isset($inspector['header']) && is_array($inspector['header']))
                    @include('components.table', ['rows' => $inspector['header']])
                @else
                    <pre>NOT FOUND</pre>
                @endif
            </div>
            <div class="form-group mb-3">
                <strong>PAYLOAD</strong>
                @if (isset($inspector['payload']) && is_array($inspector['payload']))
                    @include('components.table', ['rows' => $inspector['payload']])
                @else
                    <pre>NOT FOUND</pre>
                @endif
            </div>

        @endif

    </div>

@endsection
