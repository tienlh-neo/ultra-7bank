@extends('layout.main')

@section('main.body')

    <div class="container py-5">

        <h1>Generate sec_inf</h1>
        <hr>

        <div class="form-group py-3">
            <strong>AES (Common Key)</strong>
            <pre style="white-space: pre-wrap; word-break: break-word;">{{ $aes }}</pre>
            <strong>Common Inquiry</strong>
            <p>CommonKey will be found by searching <code>audi</code> and <code>atm_id</code> in table <code>sevenbank_common_keys</code></p>
        </div>

        <div class="row">
            <div class="col-md-6">
                <form>
                    <div class="form-group">
                        <strong>Common Key * <small>(fill CommonKey here)</small></strong>
                        <input type="text" maxlength="64" name="aes" class="form-control" value="{{ request('aes') }}">
                    </div>
                    <div class="form-group">
                        <label>1. Magnetic Stripe  [69] (磁気ストライプ情報)</label>
                        <input type="text" maxlength="69" name="magnetic_stripe" class="form-control" value="{{ request('magnetic_stripe') }}">
                    </div>
                    <div class="form-group">
                        <label>2. QR Code [23] (QRコード情報)</label>
                        <input type="text" maxlength="23" name="qr_code" class="form-control" value="{{ request('qr_code') }}">
                    </div>
                    <div class="form-group">
                        <label>3. Input PIN/Passcode [4] (入力暗証番号/入力認証番号)</label>
                        <input type="text" maxlength="4" name="passcode" class="form-control" value="{{ request('passcode') }}">
                    </div>
                    {{--
                    <div class="form-group">
                        <label>4. Reserve [4] (予備)</label>
                        <input type="text" maxlength="4" class="form-control" readonly>
                    </div>
                    --}}
                    <div class="form-group">
                        <label>5. Customer Number [16] (お客様番号)</label>
                        <input type="text" maxlength="16" name="customer_number" class="form-control" value="{{ request('customer_number') }}">
                    </div>
                    {{--
                    <div class="form-group">
                        <label>6. Reserve [10] (予備)</label>
                        <input type="text" maxlength="100" class="form-control" readonly>
                    </div>
                    --}}
                    <div class="form-group">
                        <label>7. Transaction Amount  [15] (取引金額)</label>
                        <input type="text" maxlength="15" name="amount" class="form-control" value="{{ request('amount') }}">
                    </div>
                    <div class="form-group">
                        <label>8. Transaction Type  [1] (取引種類)</label>
                        <input type="text" maxlength="1" name="transaction_type" class="form-control" value="{{ request('transaction_type') }}">
                    </div>
                    {{--
                    <div class="form-group">
                        <label>9. Reserve  [23] (予備)</label>
                        <input type="text" maxlength="23" class="form-control" readonly>
                    </div>
                    --}}
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">GENERATE</button>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <strong>RAW</strong>
                <pre style="white-space: pre-wrap; word-break: break-word;">{{ var_dump($secInf ?? '') }}</pre>
                <hr>
                <strong>RAW (HEX)</strong>
                <pre style="white-space: pre-wrap; word-break: break-word;">{{ var_dump(bin2hex($secInf ?? '')) }}</pre>
                <hr>
                <strong>ENCRYPTED</strong>
                <pre style="white-space: pre-wrap; word-break: break-word;">{{ var_dump($secInfEncrypted ?? '') }}</pre>
            </div>
        </div>

        <hr>
        <h2>Related Contents:</h2>
        <ul>
            <li><a href="https://drive.google.com/drive/folders/12TZNhIlWBB5qzOTqwwJIuRKitR96Qofi">https://drive.google.com/drive/folders/12TZNhIlWBB5qzOTqwwJIuRKitR96Qofi [P.16~]</a></li>
        </ul>
    </div>

@endsection
