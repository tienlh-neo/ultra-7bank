<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');

Route::prefix('access-token')->group(function () {
    Route::get('generate', 'AccessTokenController@issue')->name('access_token.issue');
    Route::post('send-issue', 'AccessTokenController@sendIssue')->name('access_token.send_issue');

    Route::get('revoke', 'AccessTokenController@revoke')->name('access_token.revoke');
    Route::post('send-revoke', 'AccessTokenController@sendRevoke')->name('access_token.send_revoke');
});


Route::prefix('tools')->group(function () {
    Route::get('qr-code', 'QRCodeController@index')->name('tools.qr_code');

    Route::get('jws-generator', 'JWSController@generate')->name('tools.jws_generator');
    Route::post('jws-generator', 'JWSController@generateResult')->name('tools.jws_generator');

    Route::get('jws-inspector', 'JWSController@inspect')->name('tools.jws_inspector');

    Route::get('rsa-keypair', 'RSAKeyPairController@show')->name('tools.rsa_key_pair');
    Route::post('rsa-keypair', 'RSAKeyPairController@generate')->name('tools.rsa_key_pair');

    Route::get('aes-common-key', 'AESCommonKeyController@show')->name('tools.aes_common_key');
    Route::post('aes-common-key', 'AESCommonKeyController@generate')->name('tools.aes_common_key');

    Route::get('sec-inf', 'SecInfController@show')->name('tools.sec_inf');
    Route::post('sec-inf', 'SecInfController@generate')->name('tools.sec_inf');

    Route::get('cip-key-inf', 'CipKeyInfController@show')->name('tools.cip_key_inf');

    Route::get('decode-sec-inf', 'DecodeSecInfController@show')->name('tools.decode_sec_inf');
    Route::post('decode-sec-inf', 'DecodeSecInfController@decodeSecInf')->name('tools.decode_sec_inf');
});

Route::prefix('transactions')->group(function () {
    Route::get('key-exchange', 'KeyExchangeController@show')->name('transactions.key_exchange');
    Route::post('key-exchange', 'KeyExchangeController@sendKeyExchange')->name('transactions.key_exchange');
    Route::get('transaction-detail', 'TransactionDetailController@show')->name('transactions.transaction_detail');
    Route::post('transaction-detail', 'TransactionDetailController@sendTransactionDetail')->name('transactions.transaction_detail');
    Route::get('deposit', 'DepositController@show')->name('transactions.deposit');
    Route::post('deposit', 'DepositController@sendDeposit')->name('transactions.deposit');
    Route::get('withdrawal_cancel', 'WithdrawalCancelController@show')->name('transactions.withdrawal_cancel');
    Route::post('withdrawal_cancel', 'WithdrawalCancelController@sendWithdrawalCancel')->name('transactions.withdrawal_cancel');
    Route::get('withdrawal', 'WithdrawalController@show')->name('transactions.withdrawal');
    Route::post('withdrawal', 'WithdrawalController@sendWithdrawal')->name('transactions.withdrawal');
});
