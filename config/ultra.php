<?php

return [
    'cli' => base_path('ultra-cli'),

    'jws' => [
        'secret' => env('JWS_CLIENT_CREDENTIAL'),
    ],

    'sevenbank' => [
        'base_uri' => env('SEVENBANK_BASE_URI'),
        'host'     => parse_url(env('SEVENBANK_BASE_URI'))['host'],
    ],
];
