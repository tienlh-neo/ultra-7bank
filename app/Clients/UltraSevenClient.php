<?php

namespace App\Clients;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Str;

class UltraSevenClient
{
    /**
     * @var Client
     */
    protected $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => config('ultra.sevenbank.base_uri') . 'api/v1/',
            'headers' => [
                'x-fapi-interaction-id' => (string) Str::uuid(),
            ],
        ]);
    }

    /**
     * Issue Access Token
     *
     * @param array $formParams
     *
     * @return array
     * @throws GuzzleException
     */
    public function issueAccessToken($formParams)
    {
        return $this->request('POST', 'access_token', [
            'form_params' => $formParams,
        ]);
    }

    /**
     * Issue Access Token
     *
     * @param array $formParams
     *
     * @return array
     * @throws GuzzleException
     */
    public function revokeAccessToken($formParams)
    {
        return $this->request('POST', 'revoke_token', [
            'form_params' => $formParams,
        ]);
    }

    /**
     * @param $jws
     * @return array
     * @throws GuzzleException
     */
    public function keyExchange($jws)
    {
        return $this->request('POST', 'key_exchange', [
            'body' => $jws,
        ]);
    }

    /**
     * @param $jws
     * @return array
     * @throws GuzzleException
     */
    public function transactionDetail($jws)
    {
        return $this->request('POST', 'transaction_detail', [
            'body' => $jws,
        ]);
    }

    /**
     * @param $jws
     * @return array
     * @throws GuzzleException
     */
    public function deposit($jws)
    {
        return $this->request('POST', 'deposit', [
            'body' => $jws,
        ]);
    }

    /**
     * @param $jws
     * @return array
     * @throws GuzzleException
     */
    public function withdrawalCancel($jws)
    {
        return $this->request('POST', 'withdrawal_cancel', [
            'body' => $jws,
        ]);
    }

    /**
     * @param $jws
     * @return array
     * @throws GuzzleException
     */
    public function withdrawal($jws)
    {
        return $this->request('POST', 'withdrawal', [
            'body' => $jws,
        ]);
    }

    /**
     * @param $method
     * @param $uri
     * @param array $options
     *
     * @return array
     * @throws GuzzleException
     */
    public function request($method, $uri, $options = [])
    {
        try {
            $response = $this->client->request($method, $uri, $options);

            return [
                'message'  => 'SUCCESS',
                'success'  => true,
                'headers'  => $response->getHeaders(),
                'status'   => $response->getStatusCode(),
                'response' => json_decode((string) $response->getBody(), true),
            ];
        } catch (RequestException $exception) {
            $response = $exception->getResponse();

            return [
                'message'  => $exception->getMessage(),
                'success'  => false,
                'headers'  => $response ? $response->getHeaders() : [],
                'status'   => $response ? $response->getStatusCode() : 'N/A',
                'response' => json_decode($response ? (string) $response->getBody() : '{}', true),
            ];
        } catch (Exception $exception) {
            return [
                'message'  => $exception->getMessage(),
                'success'  => false,
                'headers'  => [],
                'status'   => 'N/A',
                'response' => (object) [],
            ];
        }
    }
}
