<?php

namespace App\Http\Controllers;

class RSAKeyPairController extends Controller
{
    public function show()
    {
        $keyPair = [
            'public'  => '',
            'private' => '',
        ];

        if (file_exists(storage_path('app/keys/rsa-public.key'))) {
            $keyPair['public'] = bin2hex(file_get_contents(storage_path('app/keys/rsa-public.key')));
        }

        if (file_exists(storage_path('app/keys/rsa-private.key'))) {
            $keyPair['private'] = bin2hex(file_get_contents(storage_path('app/keys/rsa-private.key')));
        }

        return view('rsa_key_pair', compact('keyPair'));
    }

    public function generate()
    {
        $cli     = config('ultra.cli');
        $outpath = storage_path('app/keys');
        $command = "{$cli} key_make -dest \"{$outpath}\"";

        if (! file_exists($outpath)) {
            mkdir($outpath);
        }

        if (file_exists(storage_path('app/keys/rsa-public.key'))) {
            unlink(storage_path('app/keys/rsa-public.key'));
        }

        if (file_exists(storage_path('app/keys/rsa-private.key'))) {
            unlink(storage_path('app/keys/rsa-private.key'));
        }

        exec($command);

        return redirect()->route('tools.rsa_key_pair');
    }
}
