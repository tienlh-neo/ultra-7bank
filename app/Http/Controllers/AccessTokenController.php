<?php

namespace App\Http\Controllers;

use App\Clients\UltraSevenClient;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha512;
use Lcobucci\JWT\Signer\Key;

class AccessTokenController extends Controller
{
    public function issue()
    {
        $defaultClaims = [
            'iss' => 'sevenbank',
            'sub' => 'sevenbank',
            'aud' => config('ultra.sevenbank.base_uri') . 'api/v1/access_token',
            'jti' => Str::uuid()->toString(),
            'iat' => time(),
            'exp' => time() + 50,
        ];

        $defaultJWS = new Builder();
        foreach ($defaultClaims as $key => $value) {
            $defaultJWS->withClaim($key, $value);
        }
        $defaultJWS = $defaultJWS->getToken(new Sha512(), new Key(config('ultra.jws.secret')));

        return response()->view('issue_access_token', compact('defaultJWS', 'defaultClaims'));
    }

    /**
     * @param Request $request
     * @param UltraSevenClient $client
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendIssue(Request $request, UltraSevenClient $client)
    {
        $form_data = $request->all([
            'client_id',
            'grant_type',
            'scope',
            'client_assertion_type',
            'client_assertion',
        ]);

        $form_data['scope']                 = rawurlencode($form_data['scope']);
        $form_data['client_assertion_type'] = rawurlencode($form_data['client_assertion_type']);

        $response = $client->issueAccessToken($form_data);

        return response()->json($response);
    }

    public function revoke()
    {
        $defaultClaims = [
            'iss' => 'sevenbank',
            'sub' => 'sevenbank',
            'aud' => config('ultra.sevenbank.base_uri') . 'api/v1/revoke_token',
            'jti' => Str::uuid()->toString(),
            'iat' => time(),
            'exp' => time() + 50,
        ];

        $defaultJWS = new Builder();
        foreach ($defaultClaims as $key => $value) {
            $defaultJWS->withClaim($key, $value);
        }
        $defaultJWS = $defaultJWS->getToken(new Sha512(), new Key(config('ultra.jws.secret')));

        return view('revoke_access_token', compact('defaultJWS', 'defaultClaims'));
    }

    /**
     * @param Request $request
     * @param UltraSevenClient $client
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendRevoke(Request $request, UltraSevenClient $client)
    {
        $form_data = $request->all([
            'client_id',
            'grant_type',
            'client_assertion_type',
            'client_assertion',
        ]);

        $form_data['client_assertion_type'] = rawurlencode($form_data['client_assertion_type']);

        $response = $client->revokeAccessToken($form_data);

        return response()->json($response);
    }
}
