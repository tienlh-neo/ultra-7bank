<?php

namespace App\Http\Controllers;

use App\Clients\UltraSevenClient;
use Illuminate\Support\Str;

class WithdrawalCancelController extends AbstractJWSController
{
    public function show()
    {
        $fields  = $this->fields();
        $default = $this->default();

        return response()->view('withdrawal_cancel', compact('fields', 'default'));
    }

    /**
     * @param UltraSevenClient $client
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendWithdrawalCancel(UltraSevenClient $client)
    {
        $fields = array_column($this->fields(), 0);
        $claims = $this->default(request()->only($fields));
        $token  = $this->buildToken($claims);

        $response = $client->withdrawalCancel((string) $token);

        return response()->json($response);
    }

    protected function fields()
    {
        return [
            ['access_token', null, 'Access Token'],
            ['amount', 12, '12 digits - Format: 0000000amount'],
            ['tran_time', 12, 'Format: YYMMDDhhmmss'],
            ['audi_id', 4, '4 digits'],
            ['audi_subid', 2, '2 digits'],
            ['center_id', 4, '4 digits'],
            ['atm_id', 7, '7 digits'],
            ['orig_audi', 6, '6 digits = audi_id + audi_subid'],
            ['orig_trntime', 12, 'Format: YYMMDDhhmmss'],
            ['orig_reqid', 11, 'center_id + atm_id'],
            ['dest_id', 4, '4 digits'],
            ['accept_id', 4, '4 digits'],
            ['sec_inf', null, ''],
            ['cip_key_inf', null, ''],
        ];
    }

    protected function default(array $payload = [])
    {
        $current_time = time();

        return array_merge([
            'iss'          => 'sevenbank',
            'sub'          => 'sevenbank',
            'aud'          => config('ultra.sevenbank.host') . '/api/v1/withdrawal_cancel',
            'jti'          => (string) Str::uuid(),
            'iat'          => $current_time,
            'exp'          => $current_time + 50,
            'message_type' => '90100',
            'tran_flg'     => '30',
            'accept_subid' => '00',
        ], $payload);
    }
}

