<?php

namespace App\Http\Controllers;

use App\Clients\UltraSevenClient;
use Illuminate\Support\Str;

class TransactionDetailController extends AbstractJWSController
{
    public function show()
    {
        $fields  = $this->fields();
        $default = $this->default();

        return response()->view('transaction_detail', compact('fields', 'default'));
    }

    /**
     * @param UltraSevenClient $client
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendTransactionDetail(UltraSevenClient $client)
    {
        $fields = array_column($this->fields(), 0);
        $claims = $this->default(request()->only($fields));
        $token  = $this->buildToken($claims);

        $response = $client->transactionDetail((string) $token);

        return response()->json($response);
    }

    protected function fields()
    {
        return [
            ['sec_inf', null, ''],
            ['cip_key_inf', null, ''],
            ['access_token', null, 'Access Token'],
            ['tran_time', 12, 'Format: YYMMDDhhmmss'],
            ['audi_id', 4, '4 digits'],
            ['audi_subid', 2, '2 digits'],
            ['center_id', 4, '4 digits'],
            ['atm_id', 7, '7 digits'],
            ['dest_id', 4, '4 digits'],
            ['accept_id', 4, '4 digits'],
        ];
    }

    protected function default(array $payload = [])
    {
        $current_time = time();

        return array_merge([
            'iss'          => 'sevenbank',
            'sub'          => 'sevenbank',
            'aud'          => config('ultra.sevenbank.host') . '/api/v1/transaction_detail',
            'jti'          => (string) Str::uuid(),
            'iat'          => $current_time,
            'exp'          => $current_time + 50,
            'message_type' => '10100',
            'tran_flg'     => '91',
            'accept_subid' => '00',
            'amount'       => str_repeat('0', 12),
        ], $payload);
    }
}
