<?php

namespace App\Http\Controllers;

class AESCommonKeyController extends Controller
{
    public function show()
    {
        $aes = '';

        $keyPath = storage_path('app/keys/aes.key');

        if (file_exists($keyPath)) {
            $aes = bin2hex(file_get_contents($keyPath));
        }

        return response()->view('aes_common_key', compact('aes'));
    }

    public function generate()
    {
        $outpath = storage_path('app/keys');

        if (! file_exists($outpath)) {
            mkdir($outpath);
        }

        $aes = openssl_random_pseudo_bytes(32);
        file_put_contents("{$outpath}/aes.key", $aes);

        return redirect()->route('tools.aes_common_key');
    }
}
