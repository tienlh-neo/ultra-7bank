<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SevenBank\Key\SecInf;

class DecodeSecInfController  extends Controller
{
    const MAPPER = [
        'magnetic_stripe'  => [0, 69],
        'qr_code'          => [69, 23],
        'passcode'         => [92, 4],
        'reserve_01'       => [96, 4],
        'customer_number'  => [100, 16],
        'reserve_02'       => [116, 100],
        'amount'           => [216, 15],
        'transaction_type' => [231, 1],
        'reserve_03'       => [232, 23],
    ];

    public function show(Request $request)
    {
        $data = openssl_decrypt(
            base64_decode($request->sec_inf),
            'aes-256-ecb',
            hex2bin($request->common_key),
            OPENSSL_RAW_DATA
        );

        $data = $this->decodeSecInf($data);

        return response()->view('decode_sec_inf', compact('data'));
    }

    private function decodeSecInf($data)
    {
        $data = str_pad($data, 255, ' ', STR_PAD_RIGHT);
        $record = [];

        foreach (static::MAPPER as $field => $value) {
            [$start, $length] = $value;
            $string = substr($data, $start, $length);

            $record[$field] = $string;
        }

        return $record;
    }
}
