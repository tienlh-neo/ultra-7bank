<?php

namespace App\Http\Controllers;

class SecInfController extends Controller
{
    const MAPPER = [
        'magnetic_stripe'  => 69,
        'qr_code'          => 23,
        'passcode'         => 4,
        'reserve_01'       => 4,
        'customer_number'  => 16,
        'reserve_02'       => 100,
        'amount'           => 15,
        'transaction_type' => 1,
        'reserve_03'       => 23,
    ];

    public function show()
    {
        $aes = request()->get('aes', bin2hex(file_get_contents(storage_path('app/keys/aes.key'))));

        $secInf = $this->generateSecInf(request([
            'magnetic_stripe',
            'qr_code',
            'passcode',
            'customer_number',
            'amount',
            'transaction_type',
        ]));

        $secInfEncrypted = base64_encode(openssl_encrypt($secInf, 'aes-256-ecb', hex2bin($aes), OPENSSL_RAW_DATA));

        return response()->view('sec_inf', compact('aes', 'secInf', 'secInfEncrypted'));
    }

    private function generateSecInf($data)
    {
        $map = [];

        foreach (static::MAPPER as $key => $len) {
            $map[] = str_pad(trim($data[$key] ?? ''), $len, ' ', STR_PAD_LEFT);
        }

        return implode('', $map);
    }
}
