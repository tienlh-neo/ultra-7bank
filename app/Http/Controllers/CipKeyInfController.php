<?php

namespace App\Http\Controllers;

class CipKeyInfController extends Controller
{
    public function show()
    {
        $cli = config('ultra.cli');
        $publicKey = storage_path('app/keys/rsa-public.key');

        $command = "{$cli} cip_key_inf -key_exchange -rsa_public_key_path {$publicKey}";

        $cipKeyInf = exec($command);

        return response()->view('cip_key_inf', compact('cipKeyInf'));
    }
}
