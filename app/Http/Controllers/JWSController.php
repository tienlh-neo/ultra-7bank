<?php

namespace App\Http\Controllers;

use Exception;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha512;
use Lcobucci\JWT\Signer\Key;

class JWSController extends Controller
{
    public function generate()
    {
        $defaultHeader = implode(PHP_EOL, [
            'typ: JWT',
            'alg: HS512',
        ]);

        $defaultPayload = implode(PHP_EOL, [
            'iss: ',
            'sub: ',
            'aud: ',
            'jti: ',
            'iat: ',
            'exp: ',
        ]);

        $defaultSecret = config('ultra.jws.secret');

        return view('jws_generator', compact('defaultHeader', 'defaultPayload', 'defaultSecret'));
    }

    public function generateResult()
    {
        $tokenBuilder = (new Builder());

        $payload = $this->processData(request('payload'));
        $secret  = request('secret');

        foreach ($payload as $key => $value) {
            $tokenBuilder->withClaim($key, $value);
        }

        $token = (string) $tokenBuilder->getToken(new Sha512(), new Key($secret));

        return response()->json(compact('token'));
    }

    public function inspect()
    {
        $jws = request('jws');
        $inspector = [];

        if (! empty($jws)) {
            try {
                [$header, $payload] = explode('.', $jws);

                $inspector['header']  = json_decode(base64_decode($header), true);
                $inspector['payload'] = json_decode(base64_decode($payload), true);
            } catch (Exception $exception) {
                //
            }
        }

        return view('jws_inspector', compact('inspector'));
    }

    private function processData($data)
    {
        $data = trim($data);
        $rows = explode(PHP_EOL, $data);

        return array_reduce($rows, function ($merged, $row) {
            if (false !== ($pos = strpos($row, ':'))) {
                [$key] = explode(':', $row);
                $value = trim(substr($row, $pos + 1));
                $merged[trim($key)] = trim($value);
            }

            return $merged;
        }, []);
    }
}
