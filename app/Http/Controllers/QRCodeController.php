<?php

namespace App\Http\Controllers;

use Endroid\QrCode\QrCode;

class QRCodeController extends Controller
{
    public function index()
    {
        $code   = request('code');
        $qrCode = null;

        if (! empty($code)) {
            $qrGen  = new QrCode($code);
            $qrCode = $qrGen->writeDataUri();
        }

        return view('qr_code', compact('qrCode'));
    }
}
