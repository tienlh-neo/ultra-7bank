<?php

namespace App\Http\Controllers;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha512;
use Lcobucci\JWT\Signer\Key;

abstract class AbstractJWSController extends Controller
{
    protected function buildToken($claims)
    {
        /** @var Builder $token */
        $token = with(new Builder(), function (Builder $builder) use ($claims) {
            foreach ($claims as $name => $claim) {
                $builder->withClaim($name, $claim);
            }

            return $builder;
        });

        return $token->getToken(new Sha512(), new Key(config('ultra.jws.secret')));
    }

    abstract protected function fields();

    abstract protected function default(array $payload = []);
}
